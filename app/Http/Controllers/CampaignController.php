<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\Campaign\CampaignService;

class CampaignController extends Controller
{
    //
    public function register_campaign(
        Request $request,
        CampaignService $campaignService
    )
    {
        $data = $request->all();
        $registration = $campaignService->register($data);
        return $registration;
    }

    public function update_campaign(
        Request $request,
        CampaignService $campaignService,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $campaignService->update($data);
        return $registration;
    }

    public function delete_campaign(
        CampaignService $campaignService,
        $id
    )
    {
        $deleted = $campaignService->delete($id);
        return $deleted;
    }
}
