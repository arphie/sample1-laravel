<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\Clients\ClientsService;

class ClientsController extends Controller
{
    /*-------------- BOF Permissions --------------------*/
    public function register_clients(
        Request $request,
        ClientsService $clients
    )
    {
        $data = $request->all();
        $registration = $clients->register($data);
        return $registration;
    }
    
    public function update_clients(
        Request $request,
        ClientsService $clients,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $clients->update($data);
        return $registration;
    }

    public function delete_clients(
        ClientsService $clients,
        $id
    )
    {
        $deleted = $clients->delete($id);
        return $deleted;
    }
    /*-------------- EOF Permissions --------------------*/



}
