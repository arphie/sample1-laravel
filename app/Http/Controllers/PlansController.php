<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\Plans\PlansService;
use App\Http\Services\Plans\PlansUsersService;

class PlansController extends Controller
{
    /*-------------- BOF Plans --------------------*/
    public function register_plans(
        Request $request,
        PlansService $plansServices
    )
    {
        $data = $request->all();
        $registration = $plansServices->register($data);
        return $registration;
    }
    
    public function update_plans(
        Request $request,
        PlansService $plansServices,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $plansServices->update($data);
        return $registration;
    }

    public function delete_plans(
        PlansService $plansServices,
        $id
    )
    {
        $deleted = $plansServices->delete($id);
        return $deleted;
    }
    /*-------------- EOF Plans --------------------*/

    /*-------------- BOF Plans Users --------------------*/
    public function register_plans_users(
        Request $request,
        PlansUsersService $plansUserServices
    )
    {
        $data = $request->all();
        $registration = $plansUserServices->register($data);
        return $registration;
    }
    
    public function update_plans_users(
        Request $request,
        PlansUsersService $plansUserServices,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $plansUserServices->update($data);
        return $registration;
    }

    public function delete_plans_users(
        PlansUsersService $plansUserServices,
        $id
    )
    {
        $deleted = $plansUserServices->delete($id);
        return $deleted;
    }

    public function cancel_plans_users(
        PlansUsersService $plansUserServices,
        $id
    )
    {
        $deleted = $plansUserServices->delete($id);
        return $deleted;
    }
    /*-------------- EOF Plans Users --------------------*/
}
