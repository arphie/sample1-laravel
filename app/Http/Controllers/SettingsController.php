<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\Settings\SettingsService;

class SettingsController extends Controller
{
    /*-------------- BOF Settings --------------------*/
    public function register_settings(
        Request $request,
        SettingsService $settings
    )
    {
        $data = $request->all();
        $registration = $settings->register($data);
        return $registration;
    }
    
    public function update_settings(
        Request $request,
        SettingsService $settings,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $settings->update($data);
        return $registration;
    }

    public function delete_settings(
        SettingsService $settings,
        $id
    )
    {
        $deleted = $settings->delete($id);
        return $deleted;
    }
    /*-------------- EOF Settings --------------------*/



}
