<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use services
use App\Http\Services\Pixel\PixelService;
use App\Http\Services\Sync\DataSyncService;
use App\Http\Services\HtmlForms\HtmlFormsService;
use App\Http\Services\SocialLoginPages\SocialLoginPagesService;
use App\Http\Services\CustomDomain\CustomDomainService;
use App\Http\Services\LandingPages\LandingPagesService;

class SetupController extends Controller
{
    /*-------------- BOF PIXEL TRACKING --------------------*/
    /*
    * * Register pixel
    */
    public function register_pixel(
        Request $request,
        PixelService $pixelService
    )
    {
        $data = $request->all();
        $registration = $pixelService->register($data);
        return $registration;
    }
    
    public function update_Pixel(
        Request $request,
        PixelService $pixelService,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $pixelService->update($data);
        return $registration;
    }

    public function delete_Pixel(
        PixelService $pixelService,
        $id
    )
    {
        $deleted = $pixelService->delete($id);
        return $deleted;
    }
    /*-------------- EOF PIXEL TRACKING --------------------*/

    /*-------------- BOF Data Sync --------------------*/
    public function register_sync(
        Request $request,
        DataSyncService $syncService
    )
    {
        $data = $request->all();
        $registration = $syncService->register($data);
        return $registration;
    }
    
    public function update_sync(
        Request $request,
        DataSyncService $syncService,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $syncService->update($data);
        return $registration;
    }

    public function delete_sync(
        DataSyncService $syncService,
        $id
    )
    {
        $deleted = $syncService->delete($id);
        return $deleted;
    }
    /*-------------- EOF Data Sync --------------------*/

    /*-------------- BOF HTML Form --------------------*/
    public function register_html_form(
        Request $request,
        HtmlFormsService $htmlFormService
    )
    {
        $data = $request->all();
        $registration = $htmlFormService->register($data);
        return $registration;
    }
    
    public function update_html_form(
        Request $request,
        HtmlFormsService $htmlFormService,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $htmlFormService->update($data);
        return $registration;
    }

    public function delete_html_form(
        HtmlFormsService $htmlFormService,
        $id
    )
    {
        $deleted = $htmlFormService->delete($id);
        return $deleted;
    }
    /*-------------- EOF HTML Form --------------------*/

    /*-------------- BOF Social Login Pages --------------------*/
    public function register_social_login(
        Request $request,
        SocialLoginPagesService $socialLoginPages
    )
    {
        $data = $request->all();
        $registration = $socialLoginPages->register($data);
        return $registration;
    }
    
    public function update_social_login(
        Request $request,
        SocialLoginPagesService $socialLoginPages,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $socialLoginPages->update($data);
        return $registration;
    }

    public function delete_social_login(
        SocialLoginPagesService $socialLoginPages,
        $id
    )
    {
        $deleted = $socialLoginPages->delete($id);
        return $deleted;
    }
    /*-------------- EOF Social Login Pages --------------------*/

    
    /*-------------- BOF Custom Domain --------------------*/
    public function register_custom_domains(
        Request $request,
        CustomDomainService $customDomains
    )
    {
        $data = $request->all();
        $registration = $customDomains->register($data);
        return $registration;
    }
    
    public function update_custom_domains(
        Request $request,
        CustomDomainService $customDomains,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $customDomains->update($data);
        return $registration;
    }

    public function delete_custom_domains(
        CustomDomainService $customDomains,
        $id
    )
    {
        $deleted = $customDomains->delete($id);
        return $deleted;
    }
    /*-------------- EOF Custom Domain --------------------*/

    /*-------------- BOF Custom Domain --------------------*/
    public function register_landing_pages(
        Request $request,
        LandingPagesService $LandingPage
    )
    {
        $data = $request->all();
        $registration = $LandingPage->register($data);
        return $registration;
    }
    
    public function update_landing_pages(
        Request $request,
        LandingPagesService $LandingPage,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $LandingPage->update($data);
        return $registration;
    }

    public function delete_landing_pages(
        LandingPagesService $LandingPage,
        $id
    )
    {
        $deleted = $LandingPage->delete($id);
        return $deleted;
    }
    /*-------------- EOF Custom Domain --------------------*/


}
