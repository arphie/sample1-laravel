<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// list services
use App\Http\Services\Social\RegisterSocialMediaService;
use App\Http\Services\Social\UpdateSocialMediaService;
use App\Http\Services\Social\DeleteSocialMediaService;

use App\Http\Services\Social\RegisterSocialAppService;
use App\Http\Services\Social\UpdateSocialAppService;
use App\Http\Services\Social\DeleteSocialAppService;

class SocialController extends Controller
{
    // social media controller
    /*
    TODO: Refactor this soon
    */
    public function register_social_media(
        Request $request,
        RegisterSocialMediaService $registerSocial
    )
    {
        $data = $request->all();
        $registration = $registerSocial->handle($data);
        return $registration;
    }

    /*
    TODO: Refactor this soon
    */
    public function update_social_media(
        Request $request,
        UpdateSocialMediaService $updateSocial,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $updateSocial->handle($data);
        return $registration;
    }

    /*
    TODO: Refactor this soon
    */
    public function delete_social_media(
        DeleteSocialMediaService $deleteSocial,
        $id
    )
    {
        $deleted = $deleteSocial->handle($id);
        return $deleted;
    }

    // social apps
    /*
    TODO: Refactor this soon
    */
    public function register_social_app(
        Request $request,
        RegisterSocialAppService $registerSocialApps
    )
    {
        $data = $request->all();
        $registration = $registerSocialApps->handle($data);
        return $registration;
    }

    /*
    TODO: Refactor this soon
    */
    public function update_social_app(
        Request $request,
        UpdateSocialAppService $updateSocial,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $updateSocial->handle($data);
        return $registration;
    }

    /*
    TODO: Refactor this soon
    */
    public function delete_social_app(
        DeleteSocialAppService $deleteSocial,
        $id
    )
    {
        $deleted = $deleteSocial->handle($id);
        return $deleted;
    }

}
