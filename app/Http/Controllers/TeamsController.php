<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\Permissions\PermissionsService;
use App\Http\Services\Teams\TeamsService;

class TeamsController extends Controller
{
    /*-------------- BOF Permissions --------------------*/
    public function register_permissions(
        Request $request,
        PermissionsService $LandingPage
    )
    {
        $data = $request->all();
        $registration = $LandingPage->register($data);
        return $registration;
    }
    
    public function update_permissions(
        Request $request,
        PermissionsService $LandingPage,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $LandingPage->update($data);
        return $registration;
    }

    public function delete_permissions(
        PermissionsService $LandingPage,
        $id
    )
    {
        $deleted = $LandingPage->delete($id);
        return $deleted;
    }
    /*-------------- EOF Permissions --------------------*/

    /*-------------- BOF Team Members --------------------*/
    public function register_team_members(
        Request $request,
        TeamsService $teams
    )
    {
        $data = $request->all();
        $registration = $teams->register($data);
        return $registration;
    }
    
    public function update_team_members(
        Request $request,
        TeamsService $teams,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $registration = $teams->update($data);
        return $registration;
    }

    public function delete_team_members(
        TeamsService $teams,
        $id
    )
    {
        $deleted = $teams->delete($id);
        return $deleted;
    }
    /*-------------- EOF Team Members --------------------*/


}
