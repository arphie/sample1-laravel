<?php

namespace App\Http\Controllers;

// list of services
use App\Http\Services\Users\RegisterUserService;
use App\Http\Services\Users\UpdateUserService;
use App\Http\Services\Users\DeleteUserService;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    // register user

    public function register_user(
        Request $request,
        RegisterUserService $registerService
    )
    {
        $data = $request->all();
        $registration = $registerService->handle($data);
        return $registration;
    }
    
    public function update_user(
        Request $request,
        UpdateUserService $updateService,
        $id
    )
    {
        $data = $request->all();
        $data['id'] = $id;
        $update_user = $updateService->handle($data);
        return $update_user;
    }

    public function delete_user(
        $id,
        DeleteUserService $deleteUsers
    )
    {
        $delete_user = $deleteUsers->handle($id);
        return $delete_user;
    }

}
