<?php


namespace App\Http\Repositories;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

// list of models
use App\Models\CampaignModel;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class CampaignRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $campaign;
    

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        CampaignModel $campaignModel
    ){
        $this->campaign = $campaignModel;
    }

    public function register($data)
    {
        $register = $this->registerItem($this->campaign, $data);
        return $register;
    }

    public function update($data)
    {
        if(!isset($data['id']) || $data['id'] == ""){
            return [
                'status' => 500,
                'message' => 'ID is required.',
                'data' => [],
            ];
        }

        $update = $this->updateItem($this->campaign, $data);
        return $update;
    }


    public function delete($data)
    {
        $delete = $this->deleteItem($this->campaign, $data['id']);
        return $delete;
    }

    
    
}
