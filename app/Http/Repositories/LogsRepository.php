<?php


namespace App\Http\Repositories;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

// list of models
use App\Models\LogsModel;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class LogsRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    // private $permissions;
    private $Logs;
    

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        LogsModel $logsModel
    ){
        $this->Logs = $logsModel;
    }

    public function getModel($model)
    {
        // if($model == 'LogsModel'){
            return $this->Logs; // select pixel model
        // } else {
        //     return false;
        // }
    }

    public function register($data)
    {
        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $register = $this->registerItem($model, $data);
        return $register;
    }

    public function update($data)
    {
        if(!isset($data['id']) || $data['id'] == ""){
            return [
                'status' => 500,
                'message' => 'ID is required.',
                'data' => [],
            ];
        }

        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $update = $this->updateItem($model, $data);

        return $update;
    }


    public function delete($data)
    {
        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $delete = $this->deleteItem($model, $data['id']);
        return $delete;
    }

    
    
}
