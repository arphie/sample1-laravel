<?php


namespace App\Http\Repositories;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

// list of models
use App\Models\PlansModel;
use App\Models\PlansUsersModel;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class PlansRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $plans;
    private $user_plans;
    

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        PlansModel $plansModel,
        PlansUsersModel $plansUserModel
    ){
        $this->plans = $plansModel;
        $this->user_plans = $plansUserModel;
    }

    public function getModel($model)
    {
        if($model == 'PlansModel'){
            return $this->plans; // select pixel model
        } elseif($model == 'PlansUsersModel'){
            return $this->user_plans;
        } else {
            return false;
        }
    }

    public function register($data)
    {
        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $register = $this->registerItem($model, $data);
        return $register;
    }

    public function update($data)
    {
        if(!isset($data['id']) || $data['id'] == ""){
            return [
                'status' => 500,
                'message' => 'ID is required.',
                'data' => [],
            ];
        }

        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $update = $this->updateItem($model, $data);

        return $update;
    }


    public function delete($data)
    {
        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $delete = $this->deleteItem($model, $data['id']);
        return $delete;
    }

    
    
}
