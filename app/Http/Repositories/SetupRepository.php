<?php


namespace App\Http\Repositories;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

// list of models
use App\Models\PixelModel;
use App\Models\DataSyncModel;
use App\Models\HtmlFormsModel;
use App\Models\SocialPagesModel;
use App\Models\CustomDomainModel;
use App\Models\LandingPagesModel;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class SetupRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $pixel;
    private $sync;
    private $app;
    private $html_forms;
    private $social_pages;
    private $custom_domain;
    

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        PixelModel $pixelModel,
        DataSyncModel $dataSyncModel,
        HtmlFormsModel $htmlFormModels,
        SocialPagesModel $socialPages,
        CustomDomainModel $customDomainModel,
        LandingPagesModel $landingPages
    ){
        $this->pixel = $pixelModel;
        $this->sync = $dataSyncModel;
        $this->html_forms = $htmlFormModels;
        $this->social_pages = $socialPages;
        $this->custom_domain = $customDomainModel;
        $this->landing_page = $landingPages;
    }

    public function getModel($model)
    {
        if($model == 'PixelModel'){
            return $this->pixel; // select pixel model
        } elseif($model == 'DataSyncModel'){
            return $this->sync; // select sync model
        } elseif($model == 'HtmlFormsModel'){
            return $this->html_forms; // select html forms model
        } elseif($model == 'SocialPagesModel'){
            return $this->social_pages; // select social pages model
        } elseif($model == 'CustomDomainModel'){
            return $this->custom_domain; // select custom domain model
        } elseif($model == 'LandingPagesModel'){
            return $this->landing_page; // select custom domain model
        } else {
            return false;
        }
    }

    public function register($data)
    {
        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $register = $this->registerItem($model, $data);
        return $register;
    }

    public function update($data)
    {
        if(!isset($data['id']) || $data['id'] == ""){
            return [
                'status' => 500,
                'message' => 'ID is required.',
                'data' => [],
            ];
        }

        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $update = $this->updateItem($model, $data);

        return $update;
    }


    public function delete($data)
    {
        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $delete = $this->deleteItem($model, $data['id']);
        return $delete;
    }

    
    
}
