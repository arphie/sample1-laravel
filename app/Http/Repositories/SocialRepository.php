<?php


namespace App\Http\Repositories;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

// list of models

use App\Models\SocialModel;
use App\Models\SocialAppModel;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class SocialRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $social;
    private $app;
    

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        SocialModel $socialModel,
        SocialAppModel $socialAppModel
    ){
        $this->social = $socialModel;
        $this->app = $socialAppModel;
    }
    
    // social media
    public function save_socials($data)
    {
        $data['meta'] = serialize($data['meta']);

        $social = $this->social->init($data);
        
        if (!$social->validate($data)) {
            $errors = $social->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while validating the social',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }
        
        if (!$social->save()) {
            $errors = $social->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the social.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully saved the social.',
            'data' => [
                'token' => $social->id,
            ],
        ];
    }

    public function update_social($data)
    {
        if(!isset($data['id']) || $data['id'] == ""){
            return [
                'status' => 500,
                'message' => 'User ID is required.',
                'data' => [],
            ];
        }

        $data['meta'] = serialize($data['meta']);

        $social = $this->social->find($data['id']);

        // if not found, return false
        if (!$social) {
            return [
                'status' => 400,
                'message' => 'Users Details not found',
                'data' => [],
            ];
        }

        // unset id
        if (isset($data['id'])) {
            unset($data['id']);
        }

        $social->fill($data);

        //region Data insertion
        if (!$social->save()) {
            $errors = $social->getErrors();
            return [
                'status' => 500,
                'message' => 'Something went wrong with saving the Users.',
                'data' => $errors,
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully updated the Users.',
            'data' => $data,
        ];
    }

    public function delete_social($id)
    {
        $user_info = $this->social->find($id);
        if($user_info == null){
            return [
                'status' => 200,
                'message' => 'Social media already deleted.',
                'data' => [
                    'user_id' => $id
                ],
            ];
        }
        $user_info->delete();

        return [
            'status' => 200,
            'message' => 'Successfully deleted the Social media.',
            'data' => [
                'user_id' => $id
            ],
        ];
    }
    
    //social apps
    public function save_social_apps($data)
    {
        $data['social_meta'] = serialize($data['meta']);

        $social_app = $this->app->init($data);
        
        if (!$social_app->validate($data)) {
            $errors = $social_app->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while validating the social app',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }
        
        if (!$social_app->save()) {
            $errors = $social_app->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the social app.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully saved the social app.',
            'data' => [
                'token' => $social_app->id,
            ],
        ];

        exit;
    }

    public function update_social_app($data)
    {
        if(!isset($data['id']) || $data['id'] == ""){
            return [
                'status' => 500,
                'message' => 'User ID is required.',
                'data' => [],
            ];
        } 

        $data['social_meta'] = serialize($data['meta']);

        $social_app = $this->app->find($data['id']);

        // if not found, return false
        if (!$social_app) {
            return [
                'status' => 400,
                'message' => 'Social app Details not found',
                'data' => [],
            ];
        }

        // unset id
        if (isset($data['id'])) {
            unset($data['id']);
        }

        $social_app->fill($data);

        //region Data insertion
        if (!$social_app->save()) {
            $errors = $social_app->getErrors();
            return [
                'status' => 500,
                'message' => 'Something went wrong with saving the social app.',
                'data' => $errors,
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully updated the social app.',
            'data' => $data,
        ];
    }

    public function delete_social_app($id)
    {
        $user_info = $this->app->find($id);
        
        if($user_info == null){
            return [
                'status' => 200,
                'message' => 'App already deleted.',
                'data' => [
                    'user_id' => $id
                ],
            ];
        }

        $user_info->delete();

        return [
            'status' => 200,
            'message' => 'Successfully deleted the App.',
            'data' => [
                'user_id' => $id
            ],
        ];
    }
    
}
