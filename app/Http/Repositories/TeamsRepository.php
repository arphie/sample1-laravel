<?php


namespace App\Http\Repositories;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

// list of models
use App\Models\PermissionsModel;
use App\Models\TeamsModel;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class TeamsRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $permissions;
    private $teams;
    

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        PermissionsModel $permissionsModel,
        TeamsModel $teamsModel
    ){
        $this->permissions = $permissionsModel;
        $this->teams = $teamsModel;
    }

    public function getModel($model)
    {
        if($model == 'PermissionsModel'){
            return $this->permissions; // select pixel model
        } elseif($model == 'TeamsModel'){
            return $this->teams; // select pixel model
        } else {
            return false;
        }
    }

    public function register($data)
    {
        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $register = $this->registerItem($model, $data);
        return $register;
    }

    public function update($data)
    {
        if(!isset($data['id']) || $data['id'] == ""){
            return [
                'status' => 500,
                'message' => 'ID is required.',
                'data' => [],
            ];
        }

        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $update = $this->updateItem($model, $data);

        return $update;
    }


    public function delete($data)
    {
        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $delete = $this->deleteItem($model, $data['id']);
        return $delete;
    }

    
    
}
