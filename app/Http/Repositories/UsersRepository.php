<?php


namespace App\Http\Repositories;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

// list of models

use App\Models\User;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class UsersRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $user;
    

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        User $userModel
    ){
        $this->user = $userModel;
    }

    public function register($data)
    {
        if($data['password'] != $data['c_password']){
            return [
                'status' => 500,
                'message' => 'Password did not match',
                'data' => [],
            ];
        }

        $userData = $this->user->init($data);
        
        if (!$userData->validate($data)) {
            $errors = $userData->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the user',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }
        
        if (!$userData->save()) {
            $errors = $userData->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the user.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully saved the user.',
            'data' => [
                'token' => $userData->id,
            ],
        ];
    }

    public function update_user($data)
    {

        if(!isset($data['id']) || $data['id'] == ""){
            return [
                'status' => 500,
                'message' => 'User ID is required.',
                'data' => [],
            ];
        }

        

        $user_info = $this->user->find($data['id']);

        // if not found, return false
        if (!$user_info) {
            return [
                'status' => 400,
                'message' => 'Users Details not found',
                'data' => [],
            ];
        }

        // unset id
        if (isset($data['client_id'])) {
            unset($data['client_id']);
        }

        $user_info->fill($data);

        //region Data insertion
        if (!$user_info->save()) {
            $errors = $user_info->getErrors();
            return [
                'status' => 500,
                'message' => 'Something went wrong with saving the Users.',
                'data' => $errors,
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully updated the Users.',
            'data' => $data,
        ];
    }
    
    public function delete_user($id)
    {
        $user_info = $this->user->find($id);
        $user_info->delete();

        return [
            'status' => 200,
            'message' => 'Successfully updated the Users.',
            'data' => [
                'user_id' => $id
            ],
        ];
    }
    
}
