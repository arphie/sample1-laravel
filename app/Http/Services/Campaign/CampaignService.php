<?php

namespace App\Http\Services\Campaign;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\CampaignRepository;

class CampaignService extends BaseService
{   
    private $campaign;
    // private $model;

    public function __construct(
        CampaignRepository $campaignRepo
    ){
        $this->campaign = $campaignRepo;
        // $this->model = 'CampaignModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register($data)
    {   
        $data['tags'] = serialize($data['tags']);
        $social = $this->campaign->register($data);
        return $this->absorb($social);
    }

    public function update($data)
    {
        // $data['model'] = $this->model;
        $social = $this->campaign->update($data);
        return $this->absorb($social);
    }

    public function delete($id)
    {
        $data = [];
        $data['id'] = $id;
        // $data['model'] = $this->model;
        $social = $this->campaign->delete($data);
        return $this->absorb($social);
    }
}
