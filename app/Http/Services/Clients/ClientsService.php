<?php

namespace App\Http\Services\Clients;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\ClientsRepository;

class ClientsService extends BaseService
{   
    private $client;
    // private $model;

    public function __construct(
        ClientsRepository $clientsRepo
    ){
        $this->client = $clientsRepo;
        $this->model = 'ClientsModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register($data)
    {   
        $data['model'] = $this->model;
        $social = $this->client->register($data);
        return $this->absorb($social);
    }

    public function update($data)
    {
        $data['model'] = $this->model;
        $social = $this->client->update($data);
        return $this->absorb($social);
    }

    public function delete($id)
    {
        $data = [];
        $data['id'] = $id;
        $data['model'] = $this->model;
        $social = $this->client->delete($data);
        return $this->absorb($social);
    }
}
