<?php

namespace App\Http\Services\HtmlForms;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\SetupRepository;

class HtmlFormsService extends BaseService
{   
    private $setup;
    private $model;

    public function __construct(
        SetupRepository $setupRepo
    ){
        $this->setup = $setupRepo;
        $this->model = 'HtmlFormsModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register($data)
    {   
        $data['model'] = $this->model;
        $social = $this->setup->register($data);
        return $this->absorb($social);
    }

    public function update($data)
    {
        $data['model'] = $this->model;
        $social = $this->setup->update($data);
        return $this->absorb($social);
    }

    public function delete($id)
    {
        $data = [];
        $data['id'] = $id;
        $data['model'] = $this->model;
        $social = $this->setup->delete($data);
        return $this->absorb($social);
    }
}
