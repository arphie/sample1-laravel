<?php

namespace App\Http\Services\Plans;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\PlansRepository;

class PlansService extends BaseService
{   
    private $setup;
    // private $model;

    public function __construct(
        PlansRepository $plansRepo
    ){
        $this->plans = $plansRepo;
        $this->model = 'PlansModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register($data)
    {   
        $data['model'] = $this->model;
        $social = $this->plans->register($data);
        return $this->absorb($social);
    }

    public function update($data)
    {
        $data['model'] = $this->model;
        $social = $this->plans->update($data);
        return $this->absorb($social);
    }

    public function delete($id)
    {
        $data = [];
        $data['id'] = $id;
        $data['model'] = $this->model;
        $social = $this->plans->delete($data);
        return $this->absorb($social);
    }
}
