<?php

namespace App\Http\Services\Plans;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\PlansRepository;
use App\Http\Repositories\LogsRepository;

class PlansUsersService extends BaseService
{   
    private $plans;
    private $logs;
    // private $model;

    public function __construct(
        PlansRepository $plansRepo,
        LogsRepository $logsRepo
    ){
        $this->plans = $plansRepo;
        $this->logs = $logsRepo;
        $this->model = 'PlansUsersModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register($data)
    {   
        $data['model'] = $this->model;
        $social = $this->plans->register($data);
        // LogsModel
        // save activity to logs
        $logs['model'] = 'LogsModel';
        $logs['user_id'] = $data['user_id'];
        $logs['type'] = 'plans';
        $logs['value'] = $data['plan_id'];
        $logs['notes'] = 'Change Plan';
        $this->logs->register($logs);
        return $this->absorb($social);
    }

    public function update($data)
    {
        $data['model'] = $this->model;
        $social = $this->plans->update($data);
        // save activity to logs
        $logs['model'] = 'LogsModel';
        $logs['user_id'] = $data['user_id'];
        $logs['type'] = 'plans';
        $logs['value'] = $data['plan_id'];
        $logs['notes'] = 'Change Plan';
        $this->logs->register($logs);
        return $this->absorb($social);
    }

    public function delete($id)
    {
        $data = [];
        $data['id'] = $id;
        $data['model'] = $this->model;
        $social = $this->plans->delete($data);
        return $this->absorb($social);
    }
}
