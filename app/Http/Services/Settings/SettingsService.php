<?php

namespace App\Http\Services\Settings;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\SettingsRepository;

class SettingsService extends BaseService
{   
    private $settings;
    // private $model;

    public function __construct(
        SettingsRepository $settingsRepo
    ){
        $this->settings = $settingsRepo;
        $this->model = 'SettingsModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register($data)
    {   
        // $data['model'] = $this->model;
        foreach ($data['setting'] as $key => $value) {
            $xinfo = [];
            $xinfo['model'] = $this->model;
            $xinfo['user_id'] = $data['user_id'];
            $xinfo['type'] = $data['type'];
            $xinfo['key'] = $key;
            $xinfo['value'] = $value;
            
           $reps =  $this->settings->register($xinfo);
        //    dump($reps);
        }
        
        return $this->absorb([
            'status' => 200,
            'message' => 'Successfully saved',
            'data' => "",
        ]);
    }

    public function update($data)
    {
        $data['model'] = $this->model;
        $social = $this->settings->update($data);
        return $this->absorb($social);
    }

    public function delete($id)
    {
        $data = [];
        $data['id'] = $id;
        $data['model'] = $this->model;
        $social = $this->settings->delete($data);
        return $this->absorb($social);
    }
}
