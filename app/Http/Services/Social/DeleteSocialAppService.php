<?php

namespace App\Http\Services\Social;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\SocialRepository;

class DeleteSocialAppService extends BaseService
{   
    private $social;

    public function __construct(
        SocialRepository $socialRepo
    ){
        $this->social = $socialRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle($id)
    {   
        $social = $this->social->delete_social_app($id);
        return $this->absorb($social);
    }

}
