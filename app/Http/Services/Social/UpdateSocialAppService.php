<?php

namespace App\Http\Services\Social;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\SocialRepository;

class UpdateSocialAppService extends BaseService
{   
    private $social;

    public function __construct(
        SocialRepository $socialRepo
    ){
        $this->social = $socialRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle(array $data)
    {   
        $registration = $this->social->update_social_app($data);
        return $this->absorb($registration);
    }

}
