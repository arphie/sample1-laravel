<?php

namespace App\Http\Services\Teams;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\TeamsRepository;

class TeamsService extends BaseService
{   
    private $teams;
    // private $model;

    public function __construct(
        TeamsRepository $teamsRepo
    ){
        $this->teams = $teamsRepo;
        $this->model = 'TeamsModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register($data)
    {   
        $data['model'] = $this->model;
        $social = $this->teams->register($data);
        return $this->absorb($social);
    }

    public function update($data)
    {
        $data['model'] = $this->model;
        $social = $this->teams->update($data);
        return $this->absorb($social);
    }

    public function delete($id)
    {
        $data = [];
        $data['id'] = $id;
        $data['model'] = $this->model;
        $social = $this->teams->delete($data);
        return $this->absorb($social);
    }
}
