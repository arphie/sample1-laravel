<?php

namespace App\Http\Services\Users;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\UsersRepository;

class DeleteUserService extends BaseService
{   
    private $user;

    public function __construct(
        UsersRepository $userRepo
    ){
        $this->user = $userRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle($id)
    {   
        $registration = $this->user->delete_user($id);
        return $this->absorb($registration);
    }

}
