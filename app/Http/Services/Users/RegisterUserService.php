<?php

namespace App\Http\Services\Users;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\UsersRepository;

class RegisterUserService extends BaseService
{   
    private $user;

    public function __construct(
        UsersRepository $userRepo
    ){
        $this->user = $userRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle(array $data)
    {   
        $registration = $this->user->register($data);
        return $this->absorb($registration);
    }

}
