<?php

namespace App\Http\Services\Users;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\UsersRepository;

class UpdateUserService extends BaseService
{   
    private $user;

    public function __construct(
        UsersRepository $userRepo
    ){
        $this->user = $userRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle(array $data)
    {   
        $registration = $this->user->update_user($data);
        return $this->absorb($registration);
    }

}
