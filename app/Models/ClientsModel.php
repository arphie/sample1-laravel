<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class ClientsModel extends BaseModel
{
    use HasFactory, Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'clients';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'name',
        'domain',
        'available_contacts',
        'available_social_apps',
        'available_domains',
        'available_members',
        'email',
        'password'
    ];


    public $hidden = [];

    public $rules = [
        'user_id' => 'sometimes|required',
        'name' => 'sometimes|required',
        'domain' => 'sometimes|required',
        'available_contacts' => 'sometimes|required',
        'available_social_apps' => 'sometimes|required',
        'available_domains' => 'sometimes|required',
        'available_members' => 'sometimes|required',
        'email' => 'sometimes|required',
        'password' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }


    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
