<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class CustomDomainModel extends BaseModel
{
    use HasFactory, Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'custom_domains';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'for_client',
        'for_visitor',
        'root_domain',
        'use_cloudflare',
        'own_ssl',
        'use_boost_ssl',
        'status',
    ];


    public $hidden = [];

    public $rules = [
        'user_id' => 'sometimes|required',
        'for_client' => 'sometimes|required',
        'for_visitor' => 'sometimes|required',
        'root_domain' => 'sometimes|required',
        'use_cloudflare' => 'sometimes|required',
        'own_ssl' => 'sometimes|required',
        'use_boost_ssl' => 'sometimes|required',
        'status' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }


    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
