<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class LandingPagesModel extends BaseModel
{
    use HasFactory, Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'landing_pages';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'name',
        'user_id',
        'meta_title',
        'meta_desc',
        'meta_data',
        'pixel'
    ];


    public $hidden = [];

    public $rules = [
        'name' => 'sometimes|required',
        'user_id' => 'sometimes|required',
        'meta_title' => 'sometimes|required',
        'meta_desc' => 'sometimes|required',
        'meta_data' => 'sometimes|required',
        'pixel' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }


    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
