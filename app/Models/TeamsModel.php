<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class TeamsModel extends BaseModel
{
    use HasFactory, Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'teams';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'email',
        'password',
        'permissions'
    ];


    public $hidden = [];

    public $rules = [
        'user_id' => 'sometimes|required',
        'email' => 'sometimes|required',
        'password' => 'sometimes|required',
        'permissions' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }


    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
