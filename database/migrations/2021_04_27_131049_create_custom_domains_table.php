<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_domains', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('for_client')->nullable();
            $table->string('for_visitor')->nullable();
            $table->string('root_domain')->nullable();
            $table->string('use_cloudflare')->default('false');
            $table->string('own_ssl')->default('false');
            $table->string('use_boost_ssl')->default('false');
            $table->string('status')->default('saved');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_domains');
    }
}
