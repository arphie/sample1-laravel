<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// register
Route::group([
    'prefix' => 'register',
], function () {
    // users
    // TODO: Go for refactor
    Route::post("user", 'UsersController@register_user');

    // social
    // TODO: Go for refactor
    Route::post("social", 'SocialController@register_social_media');

    // social App
    // TODO: Go for refactor
    Route::post("social/apps", 'SocialController@register_social_app');

    // Pixel
    Route::post("pixel", 'SetupController@register_pixel');

    // Data Sync
    Route::post("sync", 'SetupController@register_sync');

    // HTML form
    Route::post("html", 'SetupController@register_html_form');

    // Custom Domain
    Route::post("domain", 'SetupController@register_custom_domains');

    // Social Login
    Route::post("social/login", 'SetupController@register_social_login');

    // Campaign
    Route::post("campaign", 'CampaignController@register_campaign');

    // Landing Page
    Route::post("landing", 'SetupController@register_landing_pages');

    // Permissions
    Route::post("permissions", 'TeamsController@register_permissions');

    // Teams
    Route::post("teams", 'TeamsController@register_team_members');

    // Clients
    Route::post("clients", 'ClientsController@register_clients');

    // Settings
    Route::post("settings", 'SettingsController@register_settings');

    // Plans
    Route::post("plans", 'PlansController@register_plans');

    // Plans
    Route::post("plans/users", 'PlansController@register_plans_users');
});


// update 
Route::group([
    'prefix' => 'update',
], function () {
    // users
    // TODO: Go for refactor
    Route::post("user/{id}", 'UsersController@update_user');

    // social
    // TODO: Go for refactor
    Route::post("social/{id}", 'SocialController@update_social_media');

    // social App
    // TODO: Go for refactor
    Route::post("social/apps/{id}", 'SocialController@update_social_app');

    // Pixel
    Route::post("pixel/{id}", 'SetupController@update_Pixel');

    // Data Sync
    Route::post("sync/{id}", 'SetupController@update_sync');

    // HTML form
    Route::post("html/{id}", 'SetupController@update_html_form');

    // Custom Domain
    Route::post("domain/{id}", 'SetupController@update_custom_domains');

    // Social Login
    Route::post("social/login/{id}", 'SetupController@update_social_login');

    // Campaign
     Route::post("campaign/{id}", 'CampaignController@update_campaign');

    // Landing Page
    Route::post("landing/{id}", 'SetupController@update_landing_pages');

    // Permissions
    Route::post("permissions/{id}", 'TeamsController@update_permissions');

    // Teams
    Route::post("teams/{id}", 'TeamsController@update_team_members');

    // Clients
    Route::post("clients/{id}", 'ClientsController@update_clients');

    // Settings
    Route::post("settings/{id}", 'SettingsController@update_settings');

    // Plans
    Route::post("plans/{id}", 'PlansController@update_plans');

    // Plans
    Route::post("plans/users/{id}", 'PlansController@update_plans_users');
});

// Delete 
Route::group([
    'prefix' => 'delete',
], function () {
    // users
    // TODO: Go for refactor
    Route::delete("user/{id}", 'UsersController@delete_user');

    // social
    // TODO: Go for refactor
    Route::delete("social/{id}", 'SocialController@delete_social_media');

    // social App
    // TODO: Go for refactor
    Route::delete("social/app/{id}", 'SocialController@delete_social_app');

    // social App
    Route::delete("pixel/{id}", 'SetupController@delete_Pixel');

    // Data Sync
    Route::delete("sync/{id}", 'SetupController@delete_sync');

    // HTML form
    Route::delete("html/{id}", 'SetupController@delete_html_form');

    // Custom Domain
    Route::delete("domain/{id}", 'SetupController@delete_custom_domains');

    // Social Login
    Route::delete("social/login/{id}", 'SetupController@delete_social_login');

    // campaign
    Route::delete("campaign/{id}", 'CampaignController@delete_campaign');

    // Landing Page
    Route::delete("landing/{id}", 'SetupController@delete_landing_pages');

    // Permissions
    Route::delete("permissions/{id}", 'TeamsController@delete_permissions');

    // Teams
    Route::delete("teams/{id}", 'TeamsController@delete_team_members');

    // Clients
    Route::delete("clients/{id}", 'ClientsController@delete_clients');

    // Settings
    Route::delete("settings/{id}", 'SettingsController@delete_settings');

    // Plans
    Route::delete("plans/{id}", 'PlansController@delete_plans');

    // Plans
    Route::delete("plans/users/{id}", 'PlansController@delete_plans_users');
});
